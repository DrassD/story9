from django.urls import path
from django.conf.urls import url
from .views import *


#url for app
urlpatterns = [
    path('',index,name = 'index'),
    path('get-books/',get_Google_book,name='get-google-books'),
]
